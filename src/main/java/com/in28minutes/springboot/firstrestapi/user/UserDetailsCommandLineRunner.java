package com.in28minutes.springboot.firstrestapi.user;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsCommandLineRunner implements CommandLineRunner{
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	private UserDetailsRepository repository;
	
	public UserDetailsCommandLineRunner(UserDetailsRepository repostiory) {
		super();
		this.repository = repostiory;
	}

	@Override
	public void run(String... args) throws Exception {
		repository.save(new UserDetails("Sean Mark Mira", "Admin"));
		repository.save(new UserDetails("Ria Bianca Santiago", "Admin"));
		repository.save(new UserDetails("Lexxy", "User"));
		
		List<UserDetails> users = repository.findByRole("Admin");
		
		users.forEach(user -> logger.info(user.toString()));
		
		
	}

}
