package com.in28minutes.springboot.firstrestapi.security;

import java.util.function.Function;

//Static import for filterChain method call
import static org.springframework.security.config.Customizer.withDefaults;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SpringSecurityConfiguration {
	
	@Bean
	public InMemoryUserDetailsManager createUserDetailsManager() {
		
		UserDetails userDetails1 = createNewUser("admin", "password");
		UserDetails userDetails2 = createNewUser("seanmarkmira", "seanmarkmira");
		
		return new InMemoryUserDetailsManager(userDetails1, userDetails2);
	}

	private UserDetails createNewUser(String password, String username) {
		//Where input coming in will be encode using the passwordEncoder
		Function<String, String> passwordEncoder
			= input -> passwordEncoder().encode(input);
					
		UserDetails userDetails = User.builder()
				.passwordEncoder(passwordEncoder)
				.password(password)
				.username(username)
				.roles("USER","ADMIN")
				.build();
		return userDetails;
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	//What's happening here is that in every request it will intercept the request and will utilize a filter chain
	//Do this, do this, and when everything passed, then execute request
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
		//This make sure that any request is being authenticated
		http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());
		
		//turned into basic since we just need backend for this the other option is formLogin
		http.httpBasic(withDefaults());
		
		//disable CSRF to be able to run POST and PUT request
		http.csrf().disable();
		
		//enable frames
		http.headers().frameOptions().disable();
		
		return http.build();
	}
}

